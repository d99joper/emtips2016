﻿using System;
namespace EMTips_2021.Models
{
    interface IMatchRepository
    {
        System.Linq.IQueryable<Match> GetAllMatches();
        Match GetMatch(int id);
        void Save();
    }
}
